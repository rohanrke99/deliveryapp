# Delivery Mobile APP

# Business requirement
  As an logistics firm user I want to be able to see a list of my deliveries,including receivers photo and item description.
  I also want to be able to see the delivery location on the map and full description when I click on the delivery item.

  
# User Requirements
   1. Retrieve list of deliveries from the API
   2. Display list of deliveries.
   3. Show details when user select an item in the list.
   4. Add marker on the map based on the provided lat/lng.
   
   
# Project Specification

   1. Project is developed using MVVM Clean architecture.
   2. Programming language - KOTLIN
   3. App is fetching the list of deliveries from the Network and saving them into the DB for local caching.
   4. Then the app displays the list from cache and request from server if needed.
   5. The list use a page size of 20 to fetch the list from server.
   and Libraries
   7. Android Multi-Window support added
   8. Localization added. Also flavour based localization added.
 
   

   
# Libraries used
  1. android architecture components -- for MVVM 
  2. android JetPack -- for androidx
  3. Retrofit    -- for Network api 
  4. Dagger2   -- for DI
  5. Robolectric -- UI Testing
  6. Mockito  -- Mocking framework for testing
  7. Fresco -- Facebook image management library
  8. Room  --  android architecture component for local DB
  9. RxJava -- for Reactive Programming
 10. Espresso -- For UI testing
 11. Paging -- android architecture component paging library   
 12. Chuck -- An in-app HTTP inspector for Android OkHttp clients 
   
  
# Screen shots

  ![](images/Screenshot1.png) 
  
  ![](images/Screenshot2.png) 
  
  ![](images/Screenshot3.png) 
  
  ![](images/Screenshot4.png) 
  
  
  
# How to compile
  1. Android Studio IDE (3.4.1)
  2. Android SDK (28)
  3. Select build variant for localization 
     -- devFlavourHindi,devFlavourHk,devDebug etc.. 
  
  
# Improvements / Not implemented
  
  1. Constraint Layout can be used.
  2. Transition animations can be added
  3. Can use Koin for DI in kotlin
  4. Can separate base modules and other common modules by creating separate gradle modules
  like -- logging, database, notifications etc for multiple products apps for same parent project
  5. The layout can be made suitable for phones and  immersive tablet experience.
  6. Picture in Picture mode -- Can be used in scenarios like delivery tracking on map
  
  
  
 