package com.order.delivery.presentation.deliveries


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.*
import com.order.delivery.BuildConfig
import com.order.delivery.domain.usercase.delivery.DeliveryUseCase
import com.order.delivery.rx.RxImmediateSchedulerRule
import com.order.delivery.utils.NetworkUtils
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito

class DeliveryBoundaryCallbackTest {

    @get:Rule
    var rxJavaTestHooksResetRule = RxImmediateSchedulerRule()
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var useCase: DeliveryUseCase
    lateinit var boundaryCallback: DeliveryBoundaryCallback

    @Mock
    lateinit var networkUtils: NetworkUtils


    @Before
    fun setUp() {
        useCase = Mockito.mock(DeliveryUseCase::class.java)
        networkUtils = Mockito.mock(NetworkUtils::class.java)
        boundaryCallback = DeliveryBoundaryCallback(useCase, CompositeDisposable(),networkUtils)
    }

    @Test
    fun itemAtFrontLoadTest() {
        given(useCase.fetchItemList(any(), any())).willReturn(Observable.just(mock()))

        val spy = spy(boundaryCallback)
        spy.onZeroItemsLoaded()
        verify(spy, times(1)).fetchFromNetwork(0, BuildConfig.PAGE_SIZE)
        verify(spy, times(1)).updateState(any())
    }

    @Test
    fun itemAtEndLoadTest() {
        given(useCase.fetchItemList(any(), any())).willReturn(Observable.just(mock()))

        val spy = spy(boundaryCallback)
        spy.onItemAtEndLoaded(mock())
        verify(spy, times(1)).fetchFromNetwork(any(), any())
        verify(spy, times(1)).updateState(any())
    }

    @Test
    fun refreshItemsTest() {
        given(useCase.fetchItemList(any(), any())).willReturn(Observable.just(mock()))

        val spy = spy(boundaryCallback)
        spy.onRefresh()
        assert(spy.totalCount == 0)
        verify(spy, times(1)).onZeroItemsLoaded()
        verify(spy, times(1)).fetchFromNetwork(0, BuildConfig.PAGE_SIZE)
        verify(spy, times(1)).updateState(any())
    }
}

