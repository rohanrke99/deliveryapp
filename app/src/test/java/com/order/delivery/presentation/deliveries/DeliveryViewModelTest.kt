package com.order.delivery.presentation.deliveries

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nhaarman.mockitokotlin2.*
import com.order.delivery.data.repository.State
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.domain.usercase.BaseUseCase
import com.order.delivery.domain.usercase.delivery.DeliveryUseCase
import com.order.delivery.rx.RxImmediateSchedulerRule
import com.order.delivery.utils.NetworkUtils

import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.net.UnknownHostException

@RunWith(MockitoJUnitRunner.Silent::class)
class DeliveryViewModelTest {

    @get:Rule
    var rxJavaTestHooksResetRule = RxImmediateSchedulerRule()
    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var useCase: BaseUseCase<DeliveryItem, Int>
    @Mock
    lateinit var boundaryCallback: DeliveryBoundaryCallback

    private lateinit var viewModel: DeliveryViewModel

    @Mock
    lateinit var networkUtils: NetworkUtils

    @Before
    fun setUp() {
        useCase = Mockito.mock(DeliveryUseCase::class.java)
        networkUtils = Mockito.mock(NetworkUtils::class.java)
        boundaryCallback = DeliveryBoundaryCallback(useCase, CompositeDisposable(),networkUtils = networkUtils)
        given(useCase.getItemList()).willReturn(mock())
        viewModel = DeliveryViewModel(useCase = useCase as DeliveryUseCase,networkUtils = networkUtils)
        viewModel.boundaryCallback = boundaryCallback
    }

    @Test
    fun deliveryListTest() {
        given(useCase.getItemList()).willReturn(mock())
        useCase.removeAll()
        viewModel = DeliveryViewModel(useCase = useCase as DeliveryUseCase,networkUtils = networkUtils)

        assert(viewModel.deliveryItemList.value?.size != 0)
        viewModel.boundaryCallback.state.value?.let { assert(it == State.DONE) }
    }

    @Test(expected = Exception::class)
    fun apiFailTest() {
        given(useCase.fetchItemList(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())).willThrow(
            Exception()
        )
        boundaryCallback.onZeroItemsLoaded()
    }

    @Test
    fun apiErrorTest() {
        given(useCase.fetchItemList(ArgumentMatchers.anyInt(),
                    ArgumentMatchers.anyInt())).willReturn(Observable.error(Throwable()))
        val observer = mock<Observer<String>>()
        viewModel.boundaryCallback.state.observeForever(observer)
        viewModel.boundaryCallback.onZeroItemsLoaded()
        val inOrder = Mockito.inOrder(observer)
        inOrder.verify(observer).onChanged(State.NETWORK_ERROR)
    }

    @Test
    fun networkErrorTest() {
        given(
            useCase.fetchItemList(
                ArgumentMatchers.anyInt(),
                ArgumentMatchers.anyInt()
            )
        ).willReturn(
            Observable.error(UnknownHostException())
        )
        val observer = mock<Observer<String>>()
        viewModel.boundaryCallback.state.observeForever(observer)
        viewModel.boundaryCallback.onZeroItemsLoaded()
        val inOrder = Mockito.inOrder(observer)
        inOrder.verify(observer).onChanged(State.NETWORK_ERROR)
    }

    @Test
    fun deliveryListLocalTest() {
        viewModel =
            DeliveryViewModel(useCase = useCase as DeliveryUseCase,networkUtils = networkUtils)
        viewModel.boundaryCallback.state.value?.let { assert(it == State.LOADING) }

        verify(useCase, never()).fetchItemList(any(), any())

        assert(viewModel.deliveryItemList.value?.size != 0)
        viewModel.boundaryCallback.state.value?.let { assert(it == State.DONE) }
    }

    @Test
    fun onRefreshTest() {

        assert(!viewModel.isLoading.get())
        viewModel.onRefresh()

        val spy = spy(viewModel.boundaryCallback)
        assert(viewModel.isLoading.get())

        spy.onRefresh()
        verify(spy, times(1)).onZeroItemsLoaded()
    }
}