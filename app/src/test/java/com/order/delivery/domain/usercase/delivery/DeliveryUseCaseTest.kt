package com.order.delivery.domain.usercase.delivery

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.DataSource
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.order.delivery.data.repository.delivery.DeliveryRepository
import com.order.delivery.domain.entity.DeliveryItem
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.hamcrest.CoreMatchers.notNullValue
import org.mockito.Mockito

class DeliveryUseCaseTest{

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repo  = Mockito.mock(DeliveryRepository::class.java)

    @Test
    fun testNotNull(){
        assertThat(repo, notNullValue())
    }



    @Test
    fun testGetItemList(){
        val useCase = DeliveryUseCase(repo)
        assertThat(useCase,notNullValue())
        val factory: DataSource.Factory<Int,DeliveryItem> = mock()
        whenever(useCase.getItemList()).thenReturn(factory)
        var resultItem = useCase.getItemList()
        assertTrue(resultItem==factory)
    }

    @Test
    fun testGetSingleItem(){
        val useCase = DeliveryUseCase(repo)
        assertThat(useCase,notNullValue())
        val singleItem: Single<DeliveryItem> = mock()
        whenever(useCase.getSingleItem(any())).thenReturn(singleItem)
        var resultItem = useCase.getSingleItem(any())
        assertTrue(resultItem==singleItem)
    }


    @Test
    fun testGetCount(){
        val useCase = DeliveryUseCase(repo)
        assertThat(useCase,notNullValue())
        val singleItem: Single<Int> = mock()
        whenever(useCase.getCount()).thenReturn(singleItem)
        var resultItem = useCase.getCount()
        assertTrue(resultItem==singleItem)
        assertTrue(resultItem.blockingGet()==singleItem.blockingGet())
    }


}