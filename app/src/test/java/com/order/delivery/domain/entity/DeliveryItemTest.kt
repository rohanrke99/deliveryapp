package com.order.delivery.domain.entity

import org.junit.Assert.*
import org.junit.Test

class DeliveryItemTest{

    @Test
    fun testDeliveryItem() {

        val item =
        DeliveryItem(
            id = 1, description = "Deliver documents to Andrio",
            imageUrl = "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-1.jpeg",
            location = DeliveryLocation(
                latitude = 22.335538, longitude = 114.176169,
                address = "Kowloon Tong"
            )
        )

        assertTrue(item.description=="Deliver documents to Andrio")
        assertTrue(item.id ==1)
        assertTrue(item.location!=null)
        assertTrue(item.location?.address =="Kowloon Tong")

    }
}