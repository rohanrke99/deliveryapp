package com.order.delivery.domain.entity

import org.junit.Assert.*
import org.junit.Test

class DeliveryLocationTest{


    @Test
    fun testDeliveryLocation() {

        val item =
             DeliveryLocation(
                    latitude = 22.335538, longitude = 114.176169,
                    address = "Kowloon Tong"
                )


        assertTrue(item.latitude==22.335538)
        assertTrue(item.longitude==114.176169)
        assertTrue(item.address =="Kowloon Tong")


    }
}