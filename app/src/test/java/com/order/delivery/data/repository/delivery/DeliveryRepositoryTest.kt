package com.order.delivery.data.repository.delivery

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.DataSource
import com.nhaarman.mockitokotlin2.*
import com.order.delivery.data.datasource.DeliveryDataSource
import com.order.delivery.data.mapper.DeliveryDataMapper
import com.order.delivery.data.remote.DeliveryService
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.domain.entity.DeliveryLocation
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.hamcrest.CoreMatchers.notNullValue
import org.mockito.Mockito.mock


class DeliveryRepositoryTest{

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val dataSource  = mock(DeliveryDataSource::class.java)
    private val service  = mock(DeliveryService::class.java)
    private val mapper  = mock(DeliveryDataMapper::class.java)


    @Test
    fun testNotNull(){
        assertThat(dataSource, notNullValue())
        assertThat(service, notNullValue())
        assertThat(mapper, notNullValue())
    }

    @Test
    fun testGetData(){
        val repo = DeliveryRepository(service = service,
            mapper = mapper,dataSource = dataSource)
        assertThat(repo, notNullValue())
        val factory: DataSource.Factory<Int, DeliveryItem> = mock()
        whenever(repo.get()).thenReturn(factory)
        var resultItem = repo.get()
        assertTrue(resultItem==factory)

    }

    @Test
    fun testSaveData(){
        val item = createDeliveryItem()
        val repo = DeliveryRepository(service = service,
            mapper = mapper,dataSource = dataSource)
        assertThat(repo, notNullValue())
        val singleItem: Single<DeliveryItem> = mock()

        whenever(repo.save(item)).then { doNothing() }
        whenever(repo.getById(any())).thenReturn(singleItem)
        var resultItem = repo.getById(item.id)
        assertTrue(resultItem==singleItem)
        assertTrue(resultItem.blockingGet()==singleItem.blockingGet())

    }

    private fun doNothing() {}

    private fun createDeliveryItem(): DeliveryItem {
        return DeliveryItem(
            id = 1, description = "Deliver documents to Andrio",
            imageUrl = "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-1.jpeg",
            location = DeliveryLocation(
                latitude = 22.335538, longitude = 114.176169,
                address = "Kowloon Tong"
            )
        )
    }
}