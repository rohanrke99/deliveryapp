package com.order.delivery.databinding

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.order.delivery.domain.entity.DeliveryItem

@BindingConversion
fun setVisibility(state: Boolean): Int {
    return if (state) View.VISIBLE else View.GONE
}

@BindingAdapter("imageUrl")
fun loadImage(imageView: SimpleDraweeView, url: String?) {
    if (!url.isNullOrEmpty()) {
        imageView.setImageURI(Uri.parse(url),null)
    }
}

@BindingAdapter("initMap")
fun initMap(mapView: MapView, delivery: DeliveryItem?) {
    if (delivery != null) {
        mapView.onCreate(Bundle())
        mapView.getMapAsync { googleMap ->
            MapsInitializer.initialize(mapView.context)
            val latLng = LatLng(delivery.location?.latitude!!, delivery.location.longitude!!)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
            googleMap.addMarker(MarkerOptions().position(latLng).title(delivery.location.address))
            mapView.onResume()
        }
    }
}

