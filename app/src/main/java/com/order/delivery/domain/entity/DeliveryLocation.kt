package com.order.delivery.domain.entity


data class DeliveryLocation(

    val latitude: Double?,
    val longitude: Double?,
    val address: String?
)
