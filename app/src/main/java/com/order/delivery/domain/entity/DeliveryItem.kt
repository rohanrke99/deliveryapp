package com.order.delivery.domain.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "delivery_item",
    indices = [Index(value = ["description"], unique = false)]
)
data class DeliveryItem(
    @PrimaryKey
    val id: Int,
    val description: String,
    val imageUrl: String,
    @Embedded val location: DeliveryLocation?
)
