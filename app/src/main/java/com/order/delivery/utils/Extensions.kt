package com.order.delivery.utils

import android.widget.TextView


fun TextView.setDrawableEnd(drawable :Int){
    setCompoundDrawablesWithIntrinsicBounds(0,0,drawable,0)
}