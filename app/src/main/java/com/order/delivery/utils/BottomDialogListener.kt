package com.order.delivery.utils


interface BottomDialogListener {

    fun onDialogClickListener(retry: Boolean)
}