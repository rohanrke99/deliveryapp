package com.order.delivery.di.module

import android.app.Application
import androidx.room.Room
import com.order.delivery.BuildConfig
import com.order.delivery.data.local.DeliveryDb
import com.order.delivery.data.local.dao.DeliveryItemDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Singleton
    @Provides
    fun provideDatabase(application: Application): DeliveryDb {
        return Room.databaseBuilder(
            application, DeliveryDb::class.java,
            BuildConfig.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }

    @Provides
    fun provideDeliveryDao(database: DeliveryDb): DeliveryItemDao {
        return database.dao()
    }
}