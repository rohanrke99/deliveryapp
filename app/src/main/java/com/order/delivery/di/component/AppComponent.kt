package com.order.delivery.di.component


import android.app.Application
import com.order.delivery.DeliveryApp
import com.order.delivery.di.builder.ActivityBuilder
import com.order.delivery.di.builder.FragmentBuilder
import com.order.delivery.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
        AppModule::class, ViewModelModule::class,
        ActivityBuilder::class, FragmentBuilder::class,
        DatabaseModule::class, RepoModule::class,
        NetworkModule::class]
)
interface AppComponent {

    fun inject(app: DeliveryApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
