package com.order.delivery.di.builder

import com.order.delivery.presentation.deliveries.DeliveryListFragment
import com.order.delivery.presentation.details.DeliveryDetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @ContributesAndroidInjector
    internal abstract fun contributeDeliveryListFragment(): DeliveryListFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDeliveryDetailFragment(): DeliveryDetailFragment


}
