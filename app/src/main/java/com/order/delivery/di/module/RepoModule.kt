package com.order.delivery.di.module

import com.order.delivery.data.datasource.DeliveryDataSource
import com.order.delivery.data.local.dao.DeliveryItemDao
import com.order.delivery.data.mapper.DeliveryDataMapper
import com.order.delivery.data.remote.DeliveryService
import com.order.delivery.data.repository.delivery.DeliveryRepository
import com.order.delivery.domain.datasource.BaseDataSource
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.domain.repository.BaseRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepoModule {

    @Provides
    @Singleton
    fun provideDeliveryDataSource(
        dao: DeliveryItemDao
    ): BaseDataSource<DeliveryItem, Int> {
        return DeliveryDataSource(dao)
    }

    @Provides
    @Singleton
    fun provideDeliveryRepository(
        service: DeliveryService,
        mapper: DeliveryDataMapper,
        dataSource: DeliveryDataSource
    ): BaseRepo<DeliveryItem, Int> {
        return DeliveryRepository(service, mapper, dataSource)
    }
}