package com.order.delivery.di.builder

import com.order.delivery.presentation.deliveries.DeliveryActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    internal abstract fun contributeDeliveryListActivity(): DeliveryActivity
}


