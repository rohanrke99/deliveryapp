package com.order.delivery.presentation.deliveries.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.databinding.ObservableBoolean
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.order.delivery.databinding.ItemDeliveryOrdersBinding
import com.order.delivery.databinding.LoadMoreLayoutBinding
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.presentation.LoadingViewHolder
import com.order.delivery.presentation.deliveries.OnOrderListItemClickedListener


const val VIEW_TYPE_ITEM = 0
const val VIEW_TYPE_LOADING = 1


class DeliveryListAdapter(private val listener: OnOrderListItemClickedListener) :
    PagedListAdapter<DeliveryItem,
            RecyclerView.ViewHolder>(deliveriesDiffCallback) {

    lateinit var onLoadMoreClickListener: (() -> Unit)
    private var loading = ObservableBoolean(false)
    private var loadMore = ObservableBoolean(false)

    override fun getItemViewType(position: Int): Int {
        return if (position < super.getItemCount())
            VIEW_TYPE_ITEM
        else VIEW_TYPE_LOADING


    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasLoadingFooter()) 1 else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == VIEW_TYPE_ITEM) {
            ItemHolder(
                ItemDeliveryOrdersBinding.inflate(
                    LayoutInflater.from(parent.context)
                    , parent, false
                ), listener
            )
        } else {
            LoadingViewHolder(
                LoadMoreLayoutBinding.inflate(
                    LayoutInflater.from(parent.context)
                    , parent, false
                ), onLoadMoreClickListener
            )
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position) == VIEW_TYPE_LOADING) {
            (holder as LoadingViewHolder).bind(loading.get(), loadMore.get())
        } else {
            (holder as ItemHolder).bind(getItem(position) as DeliveryItem)
        }
    }

   /* override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payload: MutableList<Any>) {
        if (payload.isNotEmpty()) {
            (holder as ItemHolder).bind(getItem(position) as DeliveryItem)
        } else {
            onBindViewHolder(holder, position)
        }
    }
    */

    class ItemHolder(
        val binding: ItemDeliveryOrdersBinding,
        private val onItemClickListener: OnOrderListItemClickedListener?
    ) :
        RecyclerView.ViewHolder(binding.root) {

        lateinit var deliveryItem: DeliveryItem
        fun bind(deliveryItem: DeliveryItem) {
            binding.item = deliveryItem
            this.deliveryItem = deliveryItem
            binding.root.setOnClickListener { onItemClickListener?.onOrderListItemClicked(deliveryItem) }
        }
    }


    companion object {
        val deliveriesDiffCallback = object : DiffUtil.ItemCallback<DeliveryItem>() {
            override fun areItemsTheSame(oldItem: DeliveryItem, newItem: DeliveryItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: DeliveryItem, newItem: DeliveryItem): Boolean {
                return oldItem == newItem
            }
        }
    }


    @VisibleForTesting
    fun getDeliveryItem(position: Int): DeliveryItem? {
        return super.getItem(position)
    }

    fun setLoading(loading: Boolean, loadMore: Boolean) {
        this.loadMore.set(loadMore)
        this.loading.set(loading)
        notifyItemChanged(super.getItemCount())
    }

    private fun hasLoadingFooter(): Boolean {
        return super.getItemCount() != 0 && (loading.get() || loadMore.get())
    }
}