package com.order.delivery.presentation.deliveries

import android.os.Bundle
import com.order.delivery.R
import com.order.delivery.presentation.BaseActivity
import org.jetbrains.annotations.TestOnly

class DeliveryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery)
        initializeViews()
        setUpToolBar(R.string.header_to_deliver, false)
    }

    private fun setUpToolBar(header: Int, showBack: Boolean) {
        title = getString(header)
        supportActionBar?.setDisplayShowHomeEnabled(showBack)
        supportActionBar?.setDisplayHomeAsUpEnabled(showBack)
    }

    private fun initializeViews() {

            supportFragmentManager.beginTransaction()
                .add(
                    R.id.frame, DeliveryListFragment.getInstance(),
                    DeliveryListFragment::class.java.simpleName
                )
                .commit()

        supportFragmentManager.addOnBackStackChangedListener {
            val count = supportFragmentManager.backStackEntryCount
            Thread.sleep(500)
            if (count == 0) {
                setUpToolBar(R.string.header_to_deliver, false)
            } else {
                setUpToolBar(R.string.header_delivery_details, true)
            }
        }
    }



    /**
     * For UI testing
     */
    @TestOnly
    fun getDeliveryListFragment(): DeliveryListFragment {
        return supportFragmentManager.fragments[0] as DeliveryListFragment
    }
}