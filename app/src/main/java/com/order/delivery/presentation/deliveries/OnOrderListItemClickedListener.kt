package com.order.delivery.presentation.deliveries

import com.order.delivery.domain.entity.DeliveryItem

interface OnOrderListItemClickedListener {

    fun onOrderListItemClicked(item: DeliveryItem)
}