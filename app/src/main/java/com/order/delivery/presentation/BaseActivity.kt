package com.order.delivery.presentation

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.order.delivery.R
import com.order.delivery.di.Injectable
import com.order.delivery.utils.BottomDialogListener
import com.order.delivery.utils.BottomDialogType
import com.order.delivery.utils.KEY_TITLE
import com.order.delivery.utils.KEY_TYPE_BOTTOM_DIALOG
import com.order.delivery.views.BottomDialogFragment
import dagger.android.AndroidInjection


abstract class BaseActivity : AppCompatActivity(), Injectable, BaseFragment.Callback {

    override fun onCreate(savedInstanceState: Bundle?) {
        performDependencyInjection()
        super.onCreate(savedInstanceState)
    }

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else
            super.onOptionsItemSelected(item)
    }

    /**
     * Inject Activity dependency
     *
     */
    private fun performDependencyInjection() {
        AndroidInjection.inject(this)
    }

    /**
     * show Toast message
     * @param text - message
     *
     */
    fun showToast(text: String) {
        val toast = Toast.makeText(this, "", Toast.LENGTH_SHORT)
        toast.duration = Toast.LENGTH_SHORT
        toast.setText(text)
        toast.setGravity(Gravity.CENTER, 0, 0)
        if (toast.view.windowVisibility == View.VISIBLE)
        else
            toast.show()
    }


    override fun onFragmentAttached() {
    }

    override fun onFragmentDetached(tag: String) {
    }

    /**
     * open custom dialog with message and heading
     * @param dialogType
     * @param listener
     */
    fun openDialog(
        @BottomDialogType dialogType: Int, title: String,
        listener: BottomDialogListener
    ) {
        try {
            val bundle = Bundle()
            bundle.putInt(KEY_TYPE_BOTTOM_DIALOG, dialogType)
            bundle.putString(KEY_TITLE, title)
            val dialogFragment = BottomDialogFragment()
            dialogFragment.show(supportFragmentManager, BottomDialogFragment::class.java.simpleName)
            dialogFragment.setOnDialogActionClickListener(listener)
            dialogFragment.isCancelable = false
            dialogFragment.arguments = bundle
        } catch (e: IllegalStateException) {
                e.printStackTrace()
        }
    }
}