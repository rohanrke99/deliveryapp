package com.order.delivery.presentation

import androidx.recyclerview.widget.RecyclerView
import com.order.delivery.databinding.LoadMoreLayoutBinding


class LoadingViewHolder(
    val binding: LoadMoreLayoutBinding,
    private val onLoadMoreClickListener: (() -> Unit)?
) :
    RecyclerView.ViewHolder(binding.root) {

    var progressBar = binding.loadMoreProgress
    fun bind(isLoading: Boolean, isLoadingMore: Boolean) {
        binding.btnLoadMore.setOnClickListener { onLoadMoreClickListener?.invoke() }
        binding.loading = isLoading
        binding.loadMore = isLoadingMore
    }
}
