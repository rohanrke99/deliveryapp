package com.order.delivery.presentation.deliveries

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.order.delivery.BuildConfig
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.domain.usercase.delivery.DeliveryUseCase
import com.order.delivery.presentation.BaseViewModel
import com.order.delivery.utils.NetworkUtils
import javax.inject.Inject

class DeliveryViewModel @Inject constructor(useCase: DeliveryUseCase,networkUtils: NetworkUtils) :
    BaseViewModel() {

    var deliveryItemList: LiveData<PagedList<DeliveryItem>>
    var boundaryCallback: DeliveryBoundaryCallback
    var deliveryItem = ObservableField<DeliveryItem>()
    var noData = ObservableBoolean(false)

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(BuildConfig.PAGE_SIZE)
            .setInitialLoadSizeHint(BuildConfig.PAGE_SIZE)
            .setEnablePlaceholders(false)
            .build()
        boundaryCallback =
            DeliveryBoundaryCallback(useCase, getDisposable(),networkUtils)
        deliveryItemList = LivePagedListBuilder(useCase.getItemList(), config)
            .setBoundaryCallback(boundaryCallback).build()
    }

    fun retry() = boundaryCallback.retry()

    fun onRefresh() {
        isLoading.set(true)
        boundaryCallback.onRefresh()
    }
}