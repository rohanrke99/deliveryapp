package com.order.delivery.presentation.deliveries

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.order.delivery.BR
import com.order.delivery.R
import com.order.delivery.data.repository.State
import com.order.delivery.databinding.FragmentDeliveryListBinding
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.presentation.BaseFragment
import com.order.delivery.presentation.deliveries.adapter.DeliveryListAdapter
import com.order.delivery.presentation.details.DeliveryDetailFragment
import com.order.delivery.utils.BottomDialogListener
import com.order.delivery.utils.BottomDialogType
import com.order.delivery.utils.setDrawableEnd
import org.jetbrains.annotations.TestOnly
import javax.inject.Inject

class DeliveryListFragment : BaseFragment<FragmentDeliveryListBinding,
        DeliveryViewModel>(), OnOrderListItemClickedListener, BottomDialogListener {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private lateinit var viewModel: DeliveryViewModel
    lateinit var adapter: DeliveryListAdapter
    private lateinit var binding: FragmentDeliveryListBinding

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_delivery_list
    }

    override fun getViewModel(): DeliveryViewModel {
        viewModel = ViewModelProviders.of(activity!!, factory)
            .get(DeliveryViewModel::class.java)

        return viewModel
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        subscribeObservers()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()
        init()

    }

    private fun init() {
        binding.noDataTv.setDrawableEnd(0)
        adapter = DeliveryListAdapter(this@DeliveryListFragment)
        adapter.onLoadMoreClickListener = { viewModel.retry() }
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.adapter = adapter
        binding.recyclerView.addItemDecoration(
            DividerItemDecoration(
                activity, DividerItemDecoration.VERTICAL
            )
        )
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.onRefresh()
        }

        binding.swipeRefresh.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )


    }

    private fun subscribeObservers() {
        viewModel.deliveryItemList.observe(this, Observer {
            adapter.submitList(it)
            adapter.notifyDataSetChanged()
            if (it != null && it.size > 0 && it[0] != null) {
                viewModel.deliveryItem.set(it[0])
                viewModel.noData.set(false)
            }else{
                viewModel.noData.set(true)
            }
            if (binding.swipeRefresh.isRefreshing) {
                binding.swipeRefresh.isRefreshing = false
            }
        })

        viewModel.boundaryCallback.state.observe(this, Observer {
            when (it) {
                State.ERROR, State.NETWORK_ERROR -> {
                    viewModel.isLoading.set(it == State.DONE)
                    adapter.setLoading(false, adapter.itemCount != 0)
                    showErrorDialog(
                        if (it == State.NETWORK_ERROR) {
                            R.string.network_error
                        } else R.string.delivery_list_error_message
                    )
                    if (binding.swipeRefresh.isRefreshing) {
                        binding.swipeRefresh.isRefreshing = false
                    }


                  //  adapter.notifyItemChanged(adapter.itemCount - 1)
                }
                State.PAGE_LOADING -> {
                    adapter.setLoading(loading = true, loadMore = false)

                  //  adapter.notifyItemChanged(adapter.itemCount - 1)
                }
                State.LOADED -> {
                    adapter.setLoading(loading = false, loadMore = false)
                    viewModel.isLoading.set(it == State.LOADING)
                    showSuccessDialog(R.string.all_data_fetched)
                }
                else -> {
                    adapter.setLoading(loading = false, loadMore = false)
                    viewModel.isLoading.set(it == State.LOADING)
                }
            }
        })
    }

    override fun onOrderListItemClicked(item: DeliveryItem) {

        viewModel.deliveryItem.set(item)
        fragmentManager?.beginTransaction()?.
            setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right,
                R.anim.enter_from_right, R.anim.exit_to_left)
            ?.add(
                R.id.frame,
                DeliveryDetailFragment.getInstance()
            )?.addToBackStack(null)
                ?.commit()

    }

    override fun showSuccessDialog(message: Int) {
        ((activity) as DeliveryActivity).openDialog(
            BottomDialogType.ALL_DATA_FETCHED,
            getString(message), this
        )
    }

    override fun showErrorDialog(message: Int) {
        ((activity) as DeliveryActivity).openDialog(
            BottomDialogType.ERROR_IN_FETCHING,
            getString(message), this
        )
    }

    override fun onDialogClickListener(retry: Boolean) {
        if (retry) {
            viewModel.retry()
        }
    }

    @TestOnly
    fun setTestViewModel(viewModel: DeliveryViewModel) {
        this.viewModel = viewModel
        subscribeObservers()
    }

    companion object {
        fun getInstance(): DeliveryListFragment {
            return DeliveryListFragment()
        }
    }

}