package com.order.delivery.presentation.details

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

import com.order.delivery.BR
import com.order.delivery.R
import com.order.delivery.databinding.FragmentDeliveryDetailBinding
import com.order.delivery.presentation.BaseFragment
import com.order.delivery.presentation.deliveries.DeliveryViewModel
import com.order.delivery.utils.setDrawableEnd
import javax.inject.Inject


class DeliveryDetailFragment : BaseFragment<FragmentDeliveryDetailBinding,
        DeliveryViewModel>() {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    private lateinit var viewModel: DeliveryViewModel
    private lateinit var binding: FragmentDeliveryDetailBinding

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_delivery_detail
    }

    override fun getViewModel(): DeliveryViewModel {
        viewModel = ViewModelProviders.of(activity!!, factory)
            .get(DeliveryViewModel::class.java)
        return viewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = getViewDataBinding()
        binding.viewModel = viewModel
        binding.deliveryDetails.orderTitle.setDrawableEnd(0)
    }

    override fun showSuccessDialog(message: Int) {
    }

    override fun showErrorDialog(message: Int) {
    }

    companion object {
        fun getInstance(): DeliveryDetailFragment {
            return DeliveryDetailFragment()
        }
    }

}