package com.order.delivery.data.remote

import com.order.delivery.data.remote.response.DeliveryResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface DeliveryService {

    @GET("deliveries/")
    fun getDeliveryList(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Single<List<DeliveryResponse>>
}