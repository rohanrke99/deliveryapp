package com.order.delivery.data.repository.delivery

import androidx.paging.DataSource
import com.order.delivery.data.mapper.DeliveryDataMapper
import com.order.delivery.data.remote.DeliveryService
import com.order.delivery.domain.datasource.BaseDataSource
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.domain.repository.BaseRepo
import io.reactivex.Single


class DeliveryRepository constructor(
    private val service: DeliveryService,
    private val mapper: DeliveryDataMapper,
    private val dataSource: BaseDataSource<DeliveryItem, Int>
) : BaseRepo<DeliveryItem, Int> {

    override fun get(offset: Int, limit: Int): Single<List<DeliveryItem>> {
        return service.getDeliveryList(offset, limit)
            .map {
                val list: List<DeliveryItem> = mapper.map(it)
                if (offset == 0) {
                    dataSource.deleteAll()
                }
                dataSource.saveEntityList(list)
                return@map list
            }
    }

    override fun get(): DataSource.Factory<Int, DeliveryItem> {
        return dataSource.getEntities()
    }

    override fun getById(id: Int): Single<DeliveryItem> {
        return dataSource.getById(id)
    }

    override fun save(entity: DeliveryItem) {
        dataSource.saveEntity(entity)
    }

    override fun save(list: List<DeliveryItem>) {
        dataSource.saveEntityList(list)
    }

    override fun update(entity: DeliveryItem) {
        dataSource.updateEntity(entity)
    }

    override fun delete(entity: DeliveryItem) {
        dataSource.deleteEntity(entity)
    }

    override fun deleteAll() {
        dataSource.deleteAll()
    }

    override fun count(): Single<Int> {
        return dataSource.count()
    }
}