package com.order.delivery.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.order.delivery.data.local.dao.DeliveryItemDao
import com.order.delivery.domain.entity.DeliveryItem

@Database(
    entities = [DeliveryItem::class], version = 1
    , exportSchema = false
)
abstract class DeliveryDb : RoomDatabase() {
    abstract fun dao(): DeliveryItemDao

}