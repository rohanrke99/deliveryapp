package com.order.delivery.data.remote.response

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class DeliveryResponse(
    @PrimaryKey
    val id: Int,
    @SerializedName("description")
    val description: String,
    @SerializedName("imageUrl")
    val imageUrl: String,
    @SerializedName("location")
    val location: DeliveryLocationResponse?)


