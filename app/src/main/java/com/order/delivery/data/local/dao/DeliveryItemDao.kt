package com.order.delivery.data.local.dao

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.order.delivery.domain.entity.DeliveryItem
import io.reactivex.Single


@Dao
interface DeliveryItemDao {

    @Query("SELECT * FROM delivery_item ORDER BY id ASC")
    fun getDeliveries(): DataSource.Factory<Int, DeliveryItem>

    @Query("SELECT * FROM delivery_item WHERE id= :deliveryId")
    fun getDelivery(deliveryId: Int): Single<DeliveryItem>

    @Query("SELECT count(*) FROM delivery_item")
    fun getCount(): Single<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDelivery(delivery: DeliveryItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllDeliveries(deliveries: List<DeliveryItem>)

    @Query("DELETE FROM delivery_item WHERE id= :deliveryId")
    fun deleteDelivery(deliveryId: Int)

    @Query("DELETE FROM delivery_item")
    fun deleteAll()
}