package com.order.delivery.data.mapper

import com.order.delivery.data.remote.response.DeliveryResponse
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.domain.entity.DeliveryLocation
import javax.inject.Inject

class DeliveryDataMapper @Inject constructor() {

    fun map(responseList: List<DeliveryResponse>): List<DeliveryItem> {
        return responseList.map { (map(it)) }
    }

    private fun map(response: DeliveryResponse): DeliveryItem {
        return DeliveryItem(
            id = response.id,
            imageUrl = response.imageUrl,
            description = response.description,
            location = DeliveryLocation(
                response.location?.latitude,
                response.location?.longitude,
                response.location?.address
            )
        )
    }
}