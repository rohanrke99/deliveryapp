package com.order.delivery.data.remote.response

import com.google.gson.annotations.SerializedName

class DeliveryLocationResponse(
    @SerializedName("lat")
    val latitude: Double?,
    @SerializedName("lng")
    val longitude: Double?,
    @SerializedName("address")
    val address: String?)