package com.order.delivery.data.datasource

import androidx.paging.DataSource
import com.order.delivery.data.local.dao.DeliveryItemDao
import com.order.delivery.domain.datasource.BaseDataSource
import com.order.delivery.domain.entity.DeliveryItem
import io.reactivex.Single
import javax.inject.Inject

class DeliveryDataSource @Inject constructor(private var dao: DeliveryItemDao) :
    BaseDataSource<DeliveryItem, Int> {
    override fun getEntities(): DataSource.Factory<Int, DeliveryItem> {
        return dao.getDeliveries()
    }

    override fun getById(id: Int): Single<DeliveryItem> {
        return dao.getDelivery(id)
    }

    override fun saveEntity(entity: DeliveryItem) {
        dao.insertDelivery(entity)
    }

    override fun saveEntityList(list: List<DeliveryItem>) {
        dao.insertAllDeliveries(list)
    }

    override fun updateEntity(entity: DeliveryItem) {
        dao.insertDelivery(entity)
    }

    override fun deleteEntity(entity: DeliveryItem) {
        dao.deleteDelivery(entity.id)
    }

    override fun deleteAll() {
        dao.deleteAll()
    }

    override fun count(): Single<Int> {
        return dao.getCount()
    }
}