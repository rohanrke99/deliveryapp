package com.order.delivery.data.local.dao

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.order.delivery.data.local.DeliveryDbTest
import com.order.delivery.utils.TestUtil

import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DeliveryItemDaoTest : DeliveryDbTest() {


    @Test
    fun insertAndReadTest() {
        val deliveryItem = TestUtil.createDeliveryItem()

        db.dao().deleteAll()

        db.dao().getCount().test()?.assertValue(0)

        db.dao().insertDelivery(deliveryItem)

        db.dao().getCount().test()?.assertValue(1)

        val item = (db.dao().getDelivery(1))

        assertThat(item, notNullValue())
        item.test().assertValue(deliveryItem)

        assertThat(item.blockingGet(), notNullValue())
        assertThat(item.blockingGet().description, notNullValue())
        assertThat(item.blockingGet().id, notNullValue())

    }

    @Test
    fun insertAllAndReadTest() {
        val list = TestUtil.createDeliveryItemList()


        db.dao().deleteAll()

        db.dao().getCount().test()?.assertValue(0)

        db.dao().insertAllDeliveries(list)

        db.dao().getCount().test()?.assertValue(list.size)

    }

}