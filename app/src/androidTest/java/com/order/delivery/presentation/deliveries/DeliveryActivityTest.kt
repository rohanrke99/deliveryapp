package com.order.delivery.presentation.deliveries

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnHolderItem
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.order.delivery.R
import com.order.delivery.data.local.DeliveryDb
import com.order.delivery.utils.CustomViewMatchers
import com.order.delivery.utils.TestUtil
import com.order.delivery.views.EspressoIdlingResource
import com.squareup.okhttp.mockwebserver.MockResponse
import com.squareup.okhttp.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class DeliveryActivityTest {

    private lateinit var deliveryDb: DeliveryDb


    @Rule
    @JvmField
    val activityRule = ActivityTestRule(DeliveryActivity::class.java, true, true)


    private lateinit var fragment: DeliveryListFragment
    private lateinit var mockServer: MockWebServer

    @Before
    fun setUp() {

        IdlingRegistry.getInstance().register(EspressoIdlingResource.getIdlingResource())

        deliveryDb = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getInstrumentation()
                .context, DeliveryDb::class.java
        ).build()
        deliveryDb.dao().deleteAll()

        mockServer = MockWebServer()
        mockServer.play(TestUtil.MOCK_PORT)


    }

    @Test
    fun mockDataSuccessTest() {
        initTest()
        mockServer.enqueue(TestUtil.createResponse("json/sample_data.json"))
        fragment =
            activityRule.activity.supportFragmentManager.findFragmentByTag(DeliveryListFragment::class.java.simpleName)
                    as DeliveryListFragment



        assert(fragment.adapter.itemCount == 2)
        assert(
            fragment.adapter.getDeliveryItem(0)?.description.equals(
                "Deliver documents to Andrio"
            )
        )

        val matcher = CustomViewMatchers.withOrderTitle(
            "0Deliver documents to Andrio"
        )


        onView((withId(R.id.recycler_view))).perform(scrollToHolder(matcher), actionOnHolderItem(matcher, click()))
    }

    @Test
    fun mockApiFailTest() {
        mockServer.enqueue(MockResponse().setResponseCode(400).setBody("Internal server error"))
        fragment =
            activityRule.activity.supportFragmentManager.findFragmentByTag(DeliveryListFragment::class.java.simpleName)
                    as DeliveryListFragment
        assert(fragment.adapter.itemCount == 0)

        onView(withText(fragment.getString(R.string.delivery_list_error_message)))
            .withFailureHandler { _, _ -> }.check(matches(isDisplayed()))
    }

    @Test
    fun viewTest() {
        initTest()

        Espresso.onView(withId(R.id.pb_frag_load_more_progress))
            .check(matches(withEffectiveVisibility(Visibility.GONE)))
        Espresso.onView(withId(R.id.recycler_view))
            .check(matches(withEffectiveVisibility(Visibility.VISIBLE)))

        onView(withId(R.id.recycler_view))
            .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(20))
    }

    @Test
    fun clickTest() {
        initTest()

        onView(withId(R.id.recycler_view))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

    }


    private fun initTest() {
        mockServer.enqueue(TestUtil.createResponse("json/sample_data.json"))

        activityRule.launchActivity(Intent())
        onView(withText("OK"))
            .withFailureHandler { _, _ -> }.check(matches(isDisplayed())).perform(click())
    }

    @After
    fun tearUp() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.getIdlingResource())
        mockServer.shutdown()

    }
    /*@Test
    fun pullToRefresh_shouldPass() {
        initTest()
        onView(withId(R.id.swipe_refresh)).perform(swipeDown())
    }
    */


}