package com.order.delivery.utils

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import com.order.delivery.presentation.deliveries.adapter.DeliveryListAdapter
import org.hamcrest.Matcher


object CustomViewMatchers {

    fun withOrderTitle(title: String): Matcher<RecyclerView.ViewHolder> {
        return object :
            BoundedMatcher<RecyclerView.ViewHolder, DeliveryListAdapter.ItemHolder>(
                DeliveryListAdapter.ItemHolder::class.java
            ) {
            override fun matchesSafely(item: DeliveryListAdapter.ItemHolder): Boolean {
                return (item.deliveryItem.id.toString().plus(
                    item.deliveryItem.description
                ) == title)
            }

            override fun describeTo(description: org.hamcrest.Description?) {
                description?.appendText("view holder with description: $title")
            }
        }
    }
}
