package com.order.delivery.utils

import androidx.test.platform.app.InstrumentationRegistry
import com.order.delivery.domain.entity.DeliveryItem
import com.order.delivery.domain.entity.DeliveryLocation
import com.squareup.okhttp.mockwebserver.MockResponse

object TestUtil {

    fun createDeliveryItem(): DeliveryItem {

        return DeliveryItem(
            id = 1, description = "Deliver documents to Andrio",
            imageUrl = "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-1.jpeg",
            location = DeliveryLocation(
                latitude = 22.335538, longitude = 114.176169,
                address = "Kowloon Tong"
            )
        )
    }

    fun createDeliveryItemList(): List<DeliveryItem> {
        val list = ArrayList<DeliveryItem>()
        for (i in 0 until 10) {
            list.add(
                DeliveryItem(
                    i + 1,
                    "",
                    "",
                    DeliveryLocation(1.0, 1.1, "")
                )
            )
        }
        return list
    }

    const val MOCK_PORT: Int = 8888

    private fun loadJson(path: String): String {
        val context = InstrumentationRegistry.getInstrumentation().context
        val stream = context.resources.assets.open(path)
        val reader = stream.bufferedReader()
        val stringBuilder = StringBuilder()
        reader.lines().forEach {
            stringBuilder.append(it)
        }
        return stringBuilder.toString()
    }

    fun createResponse(path: String): MockResponse = MockResponse()
        .setResponseCode(200)
        .setBody(loadJson(path))
}